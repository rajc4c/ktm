import 'package:flutter/material.dart';

class Contactpage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('सम्पर्क / Contact Us'),
      ),
      body: Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "काठमाडौं महानगरपालिका",
                    style: TextStyle(fontSize: 24.0),
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    'सम्पर्क ठेगाना',
                    style: TextStyle(fontSize: 22.0),
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text('बागदरबार, काठमाडौं',
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 18.0)),
                  Divider(),
                  Text(
                    'फोन तथा फ्याक्स',
                    style: TextStyle(fontSize: 22.0),
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                      'फोन: +९७७ १४२३१४८१ ',
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: TextStyle(fontSize: 18.0)),
                  // Text('फ्याक्स: ९७७-१-५२६२१२८, ५२६२१०४',
                  //     softWrap: true,
                  //     overflow: TextOverflow.ellipsis,
                  //     maxLines: 3,
                  //     style: TextStyle(fontSize: 18.0)),
                  // Text('टोल फ्रि नं.: १६६०-०१-२२२३३',
                  //     softWrap: true,
                  //     overflow: TextOverflow.ellipsis,
                  //     maxLines: 3,
                  //     style: TextStyle(fontSize: 18.0)),
                  // Text('हटलाइन नं.: १०७',
                  //     softWrap: true,
                  //     overflow: TextOverflow.ellipsis,
                  //     maxLines: 3,
                  //     style: TextStyle(fontSize: 18.0)),
                  Text('E-mail: kmcmayor@mos.com.np',
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: TextStyle(fontSize: 18.0)),
                  Text('E-mail: mayor@kathmandu.gov.np',
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: TextStyle(fontSize: 18.0)),
                ],
              ),
            ),
          ),
          
      // body: ListView(
      //   shrinkWrap: true,
      //   children: <Widget>[
      //     Card(
      //       child: Padding(
      //         padding: const EdgeInsets.all(8.0),
      //         child: Column(
      //           mainAxisAlignment: MainAxisAlignment.start,
      //           crossAxisAlignment: CrossAxisAlignment.start,
      //           children: <Widget>[
      //             Text(
      //               "CIAA Headquarter",
      //               style: TextStyle(fontSize: 24.0),
      //               softWrap: true,
      //               overflow: TextOverflow.ellipsis,
      //             ),
      //             Text(
      //               'सम्पर्क ठेगाना',
      //               style: TextStyle(fontSize: 22.0),
      //               softWrap: true,
      //               overflow: TextOverflow.ellipsis,
      //             ),
      //             Text('पो.ब.नं. ९९९६ टंगाल, काठमाडौं,नेपाल',
      //                 softWrap: true,
      //                 overflow: TextOverflow.ellipsis,
      //                 style: TextStyle(fontSize: 18.0)),
      //             Divider(),
      //             Text(
      //               'फोन तथा फ्याक्स',
      //               style: TextStyle(fontSize: 22.0),
      //               softWrap: true,
      //               overflow: TextOverflow.ellipsis,
      //             ),
      //             Text(
      //                 'फोन: ०१-५२६२१५१, ०१-५२६२११९, ०१-५२६२१७३, ०१-५२६२१०२, ०१-५२६२०५९',
      //                 softWrap: true,
      //                 overflow: TextOverflow.ellipsis,
      //                 maxLines: 3,
      //                 style: TextStyle(fontSize: 18.0)),
      //             Text('फ्याक्स: ९७७-१-५२६२१२८, ५२६२१०४',
      //                 softWrap: true,
      //                 overflow: TextOverflow.ellipsis,
      //                 maxLines: 3,
      //                 style: TextStyle(fontSize: 18.0)),
      //             Text('टोल फ्रि नं.: १६६०-०१-२२२३३',
      //                 softWrap: true,
      //                 overflow: TextOverflow.ellipsis,
      //                 maxLines: 3,
      //                 style: TextStyle(fontSize: 18.0)),
      //             Text('हटलाइन नं.: १०७',
      //                 softWrap: true,
      //                 overflow: TextOverflow.ellipsis,
      //                 maxLines: 3,
      //                 style: TextStyle(fontSize: 18.0)),
      //             Text('E-mail: akhtiyar@ntc.net.np',
      //                 softWrap: true,
      //                 overflow: TextOverflow.ellipsis,
      //                 maxLines: 3,
      //                 style: TextStyle(fontSize: 18.0)),
      //           ],
      //         ),
      //       ),
      //     ),
          
      //     buildCard(
      //         'Office of Commission for the Investigation of Abuse of Authority, Itahari',
      //         '025–586970',
      //         '025–585600'),
      //     buildCard(
      //         'Office of Commission for the Investigation of Abuse of Authority, DHANKUTA',
      //         '026-521616',
      //         '026-521606'),
      //     buildCard(
      //         'Office of Commission for the Investigation of Abuse of Authority, HETAUDA',
      //         '057-521744',
      //         '057-520369'),
      //     buildCard(
      //         'Office of Commission for the Investigation of Abuse of Authority, Bardibas',
      //         '044-550686',
      //         '044-550531'),
      //     buildCard(
      //         'Office of Commission for the Investigation of Abuse of Authority, POKHARA',
      //         '061-540451',
      //         '061-540452'),
      //     buildCard('Office of Commission for the Investigation of Abuse of Authority, Butwal', '071-540031,071-540837', '071-540823'),
      //     buildCard('Office of Commission for the Investigation of Abuse of Authority, SURKHET', '083-521960', '083-523884'),
      //     buildCard('Office of Commission for the Investigation of Abuse of Authority, Kohalpur', '081-540775', '081-540775'),
      //     buildCard('Office of Commission for the Investigation of Abuse of Authority, Kanchanpur', '099–524263', '099-524839'),
      //     buildCard(' Office of Commission for the Investigation of Abuse of Authority, DIPAYAL', '094-440156', '094-440157'),
      //   ],
      // ),
    );
  }
}

// Widget buildCard(name, phone, fax) {
//   return Card(
//     elevation: 4.0,
//     child: Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Text('$name',
//               softWrap: true,
//               overflow: TextOverflow.ellipsis,
//               maxLines: 3,
//               style: TextStyle(fontSize: 22.0)),
//           Text('फोन: $phone',
//               softWrap: true,
//               overflow: TextOverflow.ellipsis,
//               maxLines: 3,
//               style: TextStyle(fontSize: 16.0)),
//           Text('फ्याक्स: $fax',
//               softWrap: true,
//               overflow: TextOverflow.ellipsis,
//               maxLines: 3,
//               style: TextStyle(fontSize: 16.0)),
//         ],
//       ),
//     ),
//   );



// }
