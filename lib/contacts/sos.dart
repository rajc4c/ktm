import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SosPage extends StatelessWidget {
    _launchcaller(String num) async {
    String url = "tel:$num";

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget buildChild(
    BuildContext context,
    String num,
    String name

  ) {
    return Card(
      elevation: 5.0,
      child: ListTile(
        onTap: () {
           showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("फोन डायल हुदैछ ?"),
                  content: Text(
                      "तपाईको फोनमार्फत फोन नम्बर $num ($name) डायल हुदैछ।  यसलाई रद्ध गर्न \"रद्द\" वा निरन्तरता दिन \"निरन्तरता\" क्लिक गर्नुहोस्।"),
                  actions: <Widget>[
                    OutlineButton(
                      child: Text("रद्द"),
                      onPressed: () => Navigator.pop(context),
                    ),
                    OutlineButton(
                      child: Text("निरन्तरता"),
                      onPressed: () => _launchcaller(num),
                    )
                  ],
                );
              });
        },
        //contentPadding: EdgeInsets.all(5.0),
        leading: Icon(Icons.call),
        title: Text('$name', style: TextStyle(fontSize: 20.0),softWrap: true,),
        
        subtitle: Text('$num', style: TextStyle(fontSize: 24.0),softWrap: true,),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('आपतकालीन फोन नम्बरहरू'),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          buildChild(context,'100','Police Control'),
          buildChild(context,'014228435','Police Emergency Number'),
          buildChild(context,'014261945','Metropolitan Police Range (Kathmandu)'),
          buildChild(context,'014261790','Metropolitan Police Range (Kathmandu)'),
          buildChild(context,'015521207','Metropolitan Police Range (Lalitpur)'),
          buildChild(context,'016614821','Metropolitan Police Range (Bhaktapur)'),
          buildChild(context,'014260859','Paropakar Ambulance Service'),
          buildChild(context,'015545666','Lalitpur Redcross Ambulance Service'),
          buildChild(context,'014244121','Bishal Bazar Ambulance Service'),
          buildChild(context,'014228094','Redcross Ambulance Service'),
          buildChild(context,'014424875','Agrawal Sewa Centre'),
          buildChild(context,'014384881','Aasara Drug Rehabilitation Center'),
          
          
        ],
      ),
    );
  }
}
