import 'links/implinks.dart';
import 'pages/feedback/feedback.dart';
import 'pages/login.dart';
import 'pages/user/userhome.dart';
import 'package:flutter/material.dart';
import 'pages/about/aboutus.dart';
import 'pages/about/padadhikari.dart';
import 'helper/pref.dart';

Widget drawer(BuildContext context) {
  return SizedBox(
    width: MediaQuery.of(context).size.width * 0.8,
    child: new Drawer(
      child: new Container(
        color: Colors.blue[100].withOpacity(0.3),
        child: new ListView(
          children: <Widget>[
            new DrawerHeader(
              child: new Row(
                children: <Widget>[
                  Hero(
                    tag: 'logo',
                    child: new Image.asset(
                      'static/logo/ciaa.png',
                      height: 80.0,
                    ),
                  ),
                  new Expanded(
                      child: new Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: new Center(
                        child: new Text(
                      "अख्तियार दुरूपयोग अनुसन्धान आयोग",
                      style: new TextStyle(fontSize: 16.0),
                    )),
                  )),
                ],
              ),
            ),
            new ListTile(
              leading: new Icon(Icons.info_outline),
              title: new Text("हाम्रो बारेमा"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new AboutUsPage()),
                );
              },
            ),
            new Divider(),
            new ListTile(
              leading: new Icon(Icons.contact_phone),
              title: new Text("पदाधिकारीहरु"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  new MaterialPageRoute(builder: (context) => PadAdhikari()),
                );
              },
            ),
            new Divider(),
            new ListTile(
              leading: new Icon(Icons.link),
              title: new Text("महत्वपूर्ण लिंकहरू"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ImportantLinks()),
                );
              },
            ),
            new Divider(),
            new ListTile(
              leading: new Icon(Icons.add_comment),
              title: new Text("प्रतिक्रिया"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new FeedBackPage()),
                );
              },
            ),
            new Divider(),
          ],
        ),
      ),
    ),
  );
}

class UserStatus extends StatefulWidget {
  @override
  _UserStatusState createState() => _UserStatusState();
}

class _UserStatusState extends State<UserStatus> {
  String uid;
  String txt = 'लग-इन/दर्ता गर्नुहोस्';
  @override
  void initState() {
    super.initState();
    getsharedInstance().then((String ui) {
      setState(() {
        if (ui != '0') {
          txt = 'मेरो प्रोफाईल';
          uid = ui;
        } else {
          uid = ui;
          build(context);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (uid == '0') {
      return ListTile(
        leading: new Icon(Icons.account_circle),
        title: Text(txt),
        onTap: () {
          Navigator.pop(context);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginPage()));
        },
      );
    } else {
      return ListTile(
        leading: new Icon(Icons.account_circle),
        title: Text(txt),
        onTap: () {
          Navigator.pop(context);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => UserHome()));
        },
      );
    }
  }
}
