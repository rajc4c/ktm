import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'ytapi.dart';
class VideoPlayer extends StatefulWidget {
  @override
  _VideoPlayerState createState() => new _VideoPlayerState();
}


class _VideoPlayerState extends State<VideoPlayer> {
  static String key = 'AIzaSyBiigJDK2oJeNawHaCP3KwbLkUmNt-jXpI';
  YoutubeAPI ytApi = new YoutubeAPI(key);
  List<YT_API> ytResult = [];

  callApi() async {
   
    String query = "UCdgsddUoMfRF3FGKy9G3IJg";
    ytResult = await ytApi.Search(query);
    setState(() {
    
    });
  }
  @override
  void initState() {
    super.initState();
    callApi();
    
  }
  @override
  Widget build(BuildContext context) {
    // call_API();
    return new Scaffold(
          appBar: new AppBar(
            title: Text('काठमाडौं महानगरपालिका'),
          ),
          body: ytResult.length>2? Container(
            child: ListView.builder(
                itemCount: ytResult.length,
                itemBuilder: (_, int index) => ListItem(index)
            ),
          ):Center(
            child: CircularProgressIndicator(),
          )
      
    );
  }
  Widget ListItem(index){
    return GestureDetector(
      onTap: () async{
         var url = 'https://www.youtube.com/watch?v='+ytResult[index].id;
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
      },
          child: new Card(
        
        child: new Container(
          margin: EdgeInsets.symmetric(vertical: 7.0),
          padding: EdgeInsets.all(12.0),
          child:new Row(
            children: <Widget>[
              new Image.network(ytResult[index].thumbnail['default']['url'],),
              new Padding(padding: EdgeInsets.only(right: 14.0)),
              new Expanded(child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    ytResult[index].title,
                    softWrap: true,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize:18.0),
                  ),
                  new Padding(padding: EdgeInsets.only(bottom: 1.5)),
                  new Text(
                    ytResult[index].channelTitle,
                    softWrap: true,
                  ),
                  new Padding(padding: EdgeInsets.only(bottom: 3.0)),
                  new Text(
                    ytResult[index].description,
                    softWrap: true,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ]
              ))
            ],
          ),
        ),
      ),
    );
  }
}