import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'contacts/contacts.dart';
import 'links/implinks.dart';
import 'pages/homepage/home.dart';

void main() {
  runApp(CiaaApp());
}

class CiaaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //routes: ,
      debugShowCheckedModeBanner: false,
      title: 'काठमाडौं महानगरपालिका',
      home: CiaaHomePage(),
      theme: ThemeData(
        primaryColor: Colors.blue[900],
      ),
    );
  }
}

class CiaaHomePage extends StatefulWidget {
  @override
  _CiaaHomePageState createState() => _CiaaHomePageState();
}

class _CiaaHomePageState extends State<CiaaHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue[900],
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new Contactpage()),
                  );
                },
                icon: Icon(
                  FontAwesomeIcons.addressBook,
                  color: Colors.white,
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new ImportantLinks()),
                  );
                },
                icon: Icon(
                  FontAwesomeIcons.internetExplorer,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),

      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.green,
      //   onPressed: () {
      //     getsharedInstance().then((String ui) {
      //       if (ui != null && ui != '0') {
      //         Navigator.push(
      //             context, MaterialPageRoute(builder: (context) => UserHome()));
      //       } else {
      //         Navigator.push(context,
      //             MaterialPageRoute(builder: (context) => LoginPage()));
      //       }
      //     });
      //   },
      //   tooltip: "My Profile",
      //   elevation: 4.0,
      //   child: CircleAvatar(
      //     backgroundColor: Colors.transparent,
      //     child: Icon(
      //       FontAwesomeIcons.user,
      //       color: Colors.white,
      //     ),
      //   ),
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      //  drawer: drawer(context),
      appBar: AppBar(
        // leading: CircleAvatar(backgroundImage: AssetImage('static/logo/logo.png'),),
        // title: Row(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   crossAxisAlignment: CrossAxisAlignment.center,
        //   children: <Widget>[
        //     Image.asset(
        //       'static/logo/ciaa.png',
        //       width: 50.0,
        //     ),
        //     Padding(
        //       padding: const EdgeInsets.symmetric(horizontal: 18.0),
        //       child: new Text(
        //         "Kathmandu",
        //         style: TextStyle(
        //           fontSize: 22.0,
        //           fontWeight: FontWeight.bold,
        //         ),
        //       ),
        //     ),
        //   ],
        // ),
        title: Image.asset(
              'static/logo/ciaa.png',
              height: 50.0,
            ),
      ),
      body: Column(
        children: <Widget>[
          // Container(

          //   decoration: BoxDecoration(
          //     color: Colors.blue[900],
          //     borderRadius: BorderRadius.circular(0.0)

          //   ),
          //   width: MediaQuery.of(context).size.width,
          //             child: Padding(
          //               padding: const EdgeInsets.all(8.0),
          //               child: Text('अख्तियार दुरूपयोग अनुसन्धान आयोग',style: TextStyle(color: Colors.white, fontSize: 20.0,),textAlign: TextAlign.center,),
          //             ),
          // ),
          Expanded(child: HomePage()),
          // MaterialButton(
          //   onPressed: (){
          //     Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
          //   },
          //   child: Text("Login Page"),
          // ),
          //   MaterialButton(
          //   onPressed: (){
          //     Navigator.push(context, MaterialPageRoute(builder: (context)=>PostIssuePage()));
          //   },
          //   child: Text("Post Issue Page"),
          // ),
        ],
      ),
    );
  }
}
