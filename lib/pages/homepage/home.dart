
import 'package:flutter/material.dart';
import 'package:kathmandu_jangunaso/contacts/sos.dart';
import 'package:kathmandu_jangunaso/video/video.dart';
import '../postissue.dart';
import 'package:url_launcher/url_launcher.dart';
import '../sendsms.dart';
import '../news/news.dart';

class HomePage extends StatelessWidget {
  _launchcaller() async {
    const url = "tel:01439489";

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return GridView(
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      shrinkWrap: true,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => PostIssuePage()));
          },
          child: buildmenu('नयाँ गुनासो पेश गर्नुहोस् ', 'issue'),
        ),
      
        GestureDetector(
            onTap: () {
               showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("फोन डायल हुदैछ ?"),
                      content: Text(
                          "तपाईको फोनमार्फत का.म.न.पा. को फोन नम्बर +९७७ १४२३१४८१  आफ्नो गुनासो  दर्ता हुदैछ।  यसलाई रद्ध गर्न \"रद्द\" वा निरन्तरता दिन \"निरन्तरता\" क्लिक गर्नुहोस्।"),
                      actions: <Widget>[
                        OutlineButton(
                          child: Text("रद्द"),
                          onPressed: () => Navigator.pop(context),
                        ),
                        OutlineButton(
                          child: Text("निरन्तरता"),
                          onPressed: () => _launchcaller(),
                        )
                      ],
                    );
                  });
            },
            child: buildmenu('फोनमार्फत उजुरी दर्ता गर्नुहोस् ', 'call')),
        // GestureDetector(
        //     onTap: () {
        //       Navigator.push(context,
        //           MaterialPageRoute(builder: (context) => SendSMS()));
        //     },
        //     child: buildmenu('SMS मार्फत उजुरी दर्ता गर्नुहोस्', 'sms')),
        // GestureDetector(
        //     onTap: () {
        //       Navigator.push(context,
        //           MaterialPageRoute(builder: (context) => NewsPage()));
        //     },
        //     child: buildmenu('समाचार र सूचना', 'news')),
        GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => VideoPlayer()));
            },
            child: buildmenu('सुचनामुलक भिडियो', 'video')),
            GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SosPage()));
            },
            child: buildmenu('आपतकालीन फोन नम्बरहरू', 'sos')),
      ],
    );
  }
}

Widget buildmenu(title, icn) {
  return Card(
    elevation: 5.0,
    margin: EdgeInsets.all(8.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Image.asset(
            "static/icon/$icn.png",
            height: 50.0,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18.0,
              color: Colors.blue,
            ),
          ),
        )
      ],
    ),
  );
}
