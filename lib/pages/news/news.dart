import 'dart:async';
import 'dart:convert';
import 'newsdetail.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';


class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage>
     {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("सूचना तथा गतिविधि"),
          
        ),
        body:_userDetails.length>1?Container(
          child: Tab1(),
        ):Center(
            child: CircularProgressIndicator(),
          )
        );
  }
}


class Tab1 extends StatefulWidget {
  @override
  _Tab1State createState() => _Tab1State();
}

class _Tab1State extends State<Tab1> {
    Future<Null> getUserDetails() async {
    
    final response = await http.get(url);
    final responseJson = json.decode(response.body);

    setState(() {
      for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson(user));
       // print(responseJson);
      }
    }); 
  }
   @override
  void initState() {
    super.initState();
    
    _userDetails.clear();
    getUserDetails();
  }
  
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _userDetails.length,
      itemBuilder: (context, i){
        return InkWell(
          onTap: (){

             Navigator.push(context, new MaterialPageRoute(
              builder: (BuildContext context) => new NewsDetailsPage(_userDetails[i].id.toString()),
            ));
          },
                  child: Card(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                   
                Image.network("http://103.69.125.116/post_upload/"+_userDetails[i].image1, width: 100.0,height: 100.0,),
                Expanded(
                                child: Container(
                                  padding: EdgeInsets.all(12.0),
                                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                     mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text(_userDetails[i].title,softWrap: true,maxLines: 2,overflow:TextOverflow.clip,  style: TextStyle(
                            fontSize: 16.0
                          ),
                          ),
                         
                        Text(_userDetails[i].description==null?" ":_userDetails[i].description,softWrap: true,maxLines: 3,),
                        Text(_userDetails[i].date),
                        Text(_userDetails[i].category)
                      ],
                    ),
                  ),
                ),
              ],
            
            ),
          ),
        );
      },
    );
  
  }

}

List<UserDetails> _userDetails = [];

final String url = 'http://103.69.125.116/api/post_data';
class UserDetails {
  final int id;
  final String title, auther,image1,description,date, pdf,category;

  UserDetails({this.id,this.category, this.title, this.auther,this.image1,this.description, this.date,this.pdf});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
      id: json['id'],
      title: json['title'],
      auther: json['auther'],
      category: json['category'],
      image1: json['image'],
      description:json['description'],
      date: json['newsdate'],
      pdf:json['pdf']

    );
  }
}

