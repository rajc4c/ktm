import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

Map _userDetails;

final String url = 'http://103.69.125.116/api/post_datum?id=';

class NewsDetailsPage extends StatefulWidget {
  NewsDetailsPage(this.id);
  final String id;

  @override
  _NewsDetailsPageState createState() => _NewsDetailsPageState();
}

class _NewsDetailsPageState extends State<NewsDetailsPage> {
  Future<Null> getUserDetails() async {
    // print(url + widget.id);
    final response = await http.get(url + widget.id);
    final responseJson = json.decode(response.body);
    // print(responseJson);

//print(a['title']);
    setState(() {
      _userDetails = responseJson;
    });
  }

  @override
  void initState() {
    super.initState();
    // _userDetails.clear();
    getUserDetails();

  }
  @override
    void dispose() {
      _userDetails=null;
      super.dispose();
    }

  @override
  Widget build(BuildContext context) {
    return _userDetails != null
        ? Scaffold(
            appBar: AppBar(
              title: Text(_userDetails['title']),
            ),
            body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    _userDetails['title'],
                    softWrap: true,
                    maxLines: 2,
                    overflow: TextOverflow.clip,
                    style: TextStyle(fontSize: 18.0),
                  ),
                   Text("Category : " + _userDetails['category']),
                  Text("Created at: " + _userDetails['newsdate']),
                  Expanded(
                    child: InkWell(
                      onTap: () async {
                        var url =
                            "http://103.69.125.116/post_upload/" +
                                _userDetails['image'];
                        if (await canLaunch(url)) {
                          await launch(url,
                              forceWebView: false, forceSafariVC: true);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Image.network(
                          "http://103.69.125.116/post_upload/" +
                              _userDetails['image'], fit: BoxFit.fitWidth,),
                    ),
                  ),
                  InkWell(
                      onTap: () async {
                        var url =
                            "http://103.69.125.116/post_upload/" +
                                _userDetails['image'];
                        if (await canLaunch(url)) {
                          await launch(url,
                              forceWebView: false, forceSafariVC: true);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(_userDetails['image'],style: TextStyle(color: Colors.blue),),
                      ),
                    ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Text(
                        _userDetails['description'] == null
                            ? " "
                            : _userDetails['description'],
                        softWrap: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        : Scaffold(
            body: Container(
              child: new Dialog(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      new CircularProgressIndicator(),
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: new Text("लोड हुँदैछ"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
