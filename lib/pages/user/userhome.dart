import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../../helper/pref.dart';

String uid = ' ', name = " ", phone = " ", address = " ", email = " ";

class UserHome extends StatefulWidget {
  @override
  _UserHomeState createState() => _UserHomeState();
}

class _UserHomeState extends State<UserHome> {
  Future<Null> getPostDetails() async {
    final response = await http.get(url);
    final responseJson = json.decode(response.body);

    setState(() {
      for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson(user));
        
      }
    });
  }

  getuserDetails() async {
    uid = await getsharedInstance();
    name = await getsharedname();
    phone = await getsharedphone();
    address = await getshareadd();
    email = await getsharedemail();
  }

  @override
  void initState() {

    getuserDetails();

    _userDetails.clear();
    getPostDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("मेरो प्रोफाइल"),
        actions: <Widget>[
          GestureDetector(
             onTap: () async {
              await setsharedInstance(uid: '0');
              Navigator.pop(context);
              setState(() {});
            },
                      child: Row(
              children: <Widget>[
Text("Log out"),
Icon(Icons.exit_to_app),
              ],
              
             
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            userifo(),
            Expanded(child: submittedissue()),
          ],
        ),
      ),
    );
  }
}

Widget userifo() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        border: Border.all(color: Colors.greenAccent)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Image.asset(
          'static/img/avatar.png',
          width: 90.0,
          height: 90.0,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  name != null ? name : '',
                  style: TextStyle(fontSize: 20.0),
                ),

                Text(
                  phone,
                  style: TextStyle(fontSize: 20.0),
                ),

                Text(address != null ? address : ''),
                Text(email != null ? email : ''),
                // OutlineButton(
                //   onPressed: (){},
                //   child: Text("View / Update Profile"),
                // ),
              ],
            ),
          ),
        )
      ],
    ),
  );
}

List<UserDetails> _userDetails = [];

final String url = 'https://103.69.125.116/api/post_data';

class UserDetails {
  final int id;
  final String title, auther, image1, description, date, pdf, category;

  UserDetails(
      {this.id,
      this.category,
      this.title,
      this.auther,
      this.image1,
      this.description,
      this.date,
      this.pdf});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
        id: json['id'],
        title: json['title'],
        auther: json['auther'],
        category: json['category'],
        image1: json['image'],
        description: json['description'],
        date: json['newsdate'],
        pdf: json['pdf']);
  }
}

Widget submittedissue() {
  return ListView.builder(
    itemCount: _userDetails.length,
    itemBuilder: (context, i) {
      return InkWell(
        onTap: () {
          //  Navigator.push(context, new MaterialPageRoute(
          //   builder: (BuildContext context) => new NewsDetailsPage(_userDetails[i].id.toString()),
          // ));
        },
        child: Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.network(
                "http://103.69.125.116/post_upload/" +
                    _userDetails[i].image1,
                width: 100.0,
                height: 100.0,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        _userDetails[i].title,
                        softWrap: true,
                        maxLines: 2,
                        overflow: TextOverflow.clip,
                        style: TextStyle(fontSize: 16.0),
                      ),
                      Text(
                        _userDetails[i].description == null
                            ? " "
                            : _userDetails[i].description,
                        softWrap: true,
                        maxLines: 3,
                      ),
                      Text(_userDetails[i].date),
                      Text(_userDetails[i].category)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
