import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  Map<String,String> _formdata ={} ;

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {

    final logo=Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        // Hero(
        //   tag: 'hero',
        //   child: CircleAvatar(
        //     backgroundColor: Colors.transparent,
        //     //backgroundImage: AssetImage('static/logo/logo.png'),
        //     radius: 50.0,
        //     child: Image.asset('static/logo/logo.png'),
        //   ),
        //   ),
          Hero(
          tag: 'hero1',
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            //backgroundImage: AssetImage('static/logo/logo.png'),
            radius: 50.0,
            child: Image.asset('static/logo/ciaa.png'),
          ),
          ),
      ],
    );
    final email = TextFormField(
      onSaved: (val){
        _formdata['email']=val;
      },
      validator: (value) {
        Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
              if (!regex.hasMatch(value)&&value.length!=10) {
                return 'कृपया मान्य ई-मेल वा मोबाइल नम्बर लेख्नुहोस्';
              }
            },
      
      autofocus: false,
      // initialValue: 'myemailid@gmail.com',
      decoration: InputDecoration(
        hintText: 'इमेल / मोबाइल नम्बर',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))
      ),
    );

    final loginbutton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(32.0),
        elevation: 5.0,
        shadowColor: Colors.lightBlueAccent.shade100,
        child: MaterialButton(
          minWidth: 200.0,
          height: 48.0,
          onPressed: (){
             if (_formKey.currentState.validate()) {
                  // If the form is valid, we want to show a Snackbar
                   showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context){
                      return AlertDialog(
                        title: Text("डेटा प्रशोधन हुँदैछ"),
                        content: LinearProgressIndicator(),
                      );
                    }
                  );
                }
          },
          child: Text("पासवर्ड रिसेट",style: TextStyle(color: Colors.white),),
          color: Colors.lightBlueAccent,

        ),
      ),
    );
    return Scaffold(
      appBar: AppBar(title: Text("पासवर्ड रिसेट"),),
      body: Center(
        child: Form(
          key: _formKey,
                  child: ListView(
            shrinkWrap: true,
            
            padding: EdgeInsets.only(left: 24.0,right: 24.0),
              children: <Widget>[
          logo,
          SizedBox(height: 38.0,),
          email,
          SizedBox(height: 18.0,),
       
          loginbutton,
       SizedBox(height: 58.0,),
          
              ],
            ),
        ),
      ),
      
    );
  }
}