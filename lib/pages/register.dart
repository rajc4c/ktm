
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:kathmandu_jangunaso/main.dart';
import 'dart:async';

import 'package:kathmandu_jangunaso/pages/login.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  Map<String, String> _formdata = {};
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final name = TextFormField(
      onSaved: (val) {
        _formdata['name'] = val;
      },
      validator: (value) {
        if (value.isEmpty) {
          return 'कृपया आफ्नो नाम लेख्नुहोस्';
        }
      },
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'पूरा नाम *',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );
    final address = TextFormField(
      onSaved: (val) {
        _formdata['address'] = val;
      },
      keyboardType: TextInputType.multiline,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'ठेगाना',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );
    final mobile = TextFormField(
      onSaved: (val) {
        _formdata['phone'] = val;
      },
      validator: (value) {
        if (value.length != 10) {
          return 'कृपया वैध मोबाइल नम्बर लेख्नुहोस्';
        }
      },
      keyboardType: TextInputType.phone,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'मोबाइल नम्बर *',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );
    final email = TextFormField(
      onSaved: (val) {
        _formdata['email'] = val;
      },
      validator: (value) {
        if (value.isNotEmpty) {
          Pattern pattern =
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
          RegExp regex = new RegExp(pattern);
          if (!regex.hasMatch(value)) {
            return 'कृपया मान्य ईमेल लेख्नुहोस्';
          }
        } else {
          return 'कृपया मान्य ईमेल लेख्नुहोस्';
        }
      },
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      // initialValue: 'myemailid@gmail.com',
      decoration: InputDecoration(
          hintText: 'इमेल ठेगाना *',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );
    final password = TextFormField(
      onSaved: (val) {
        _formdata['password'] = val;
        _formdata['password_confirmation'] = val;
      },
      validator: (value) {
        if (value.length < 6) {
          return 'पासवर्ड कम्तिमा 6 अक्षर हुनुपर्दछ';
        }
      },
      obscureText: true,
      autofocus: false,
      // initialValue: 'my password',
      decoration: InputDecoration(
          hintText: 'पासवर्ड *',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );

    final registerbutton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(32.0),
        elevation: 5.0,
        shadowColor: Colors.lightBlueAccent.shade100,
        child: MaterialButton(
          minWidth: 200.0,
          height: 48.0,
          onPressed: () async {
            _formKey.currentState.save();
            if (_formKey.currentState.validate()) {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("डेटा प्रशोधन हुँदैछ..."),
                      content: LinearProgressIndicator(),
                    );
                  });

              Dio dio = Dio();
              dio.options.connectTimeout = 15000; //15s
              dio.options.receiveTimeout = 15000;

              FormData formData = new FormData.from(_formdata);
              var response = await dio.post(
                  'http://103.69.125.116/api/register_api',
                  data: formData);

              // print(response.data);
              Navigator.pop(context);
              if (response.data.toString() == 'registration successfull') {
                
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("सफलता !!"),
                        content: Text(response.data.toString().toUpperCase()+'\nकृपया लग इन गर्नुहोस् '),
                      );
                    });
                Timer(Duration(seconds: 1), () {
                  Navigator.pop(context);
                   Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
                  // Navigator.popUntil(context, ModalRoute.withName('/'));
                });
              } else {
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("त्रुटि !!"),
                        content: Text(response.data.toString()),
                      );
                    });
              }
            }
          },
          child: Text(
            "दर्ता गर्नुहोस् ",
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.lightBlueAccent,
        ),
      ),
    );
    final guest = OutlineButton(
      child: Text(
        "गृहपृष्ठमा फर्किनुहोस्",
        style: TextStyle(color: Colors.black),
      ),
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => CiaaApp()));
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("नयाँ खाता खोल्नुहोस्"),
      ),
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Form(
            key: _formKey,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
              children: <Widget>[
                Text(
                  "नयाँ खाता बनाउन कृपया तलको फारम भर्नुहोस्",
                  softWrap: true,
                ),
                SizedBox(
                  height: 18.0,
                ),
                name,
                SizedBox(
                  height: 18.0,
                ),
                address,
                SizedBox(
                  height: 18.0,
                ),
                mobile,
                SizedBox(
                  height: 18.0,
                ),
                email,
                SizedBox(
                  height: 18.0,
                ),
                password,
                SizedBox(
                  height: 18.0,
                ),
                registerbutton,
                SizedBox(
                  height: 24.0,
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
            child: guest,
          )
        ],
      ),
    );
  }
}
