import 'dart:async';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

Dio dio = new Dio();
Response response = Response();
Map<String, dynamic> _formdata = {};

class FeedBackPage extends StatefulWidget {
  @override
  _FeedBackPageState createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {
  bool sucess = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final logo = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Hero(
          tag: 'hero',
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            //backgroundImage: AssetImage('static/logo/logo.png'),
            radius: 50.0,
            child: Image.asset('static/logo/logo.png'),
          ),
        ),
        Hero(
          tag: 'logo',
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            //backgroundImage: AssetImage('static/logo/logo.png'),
            radius: 50.0,
            child: Image.asset('static/logo/ciaa.png'),
          ),
        ),
      ],
    );
    final nameField = TextFormField(
      onSaved: (val) {
        _formdata['name'] = val;
      },
      validator: (value) {
        if (value.length == 0) {
          return 'नाम खाली छ';
        }
      },
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'पुरा नाम',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );
    final messageField = TextFormField(
      onSaved: (val) {
        _formdata['message'] = val;
      },
      validator: (value) {
        if (value.length < 20) return 'सुझाव कम्तिमा 20 अक्षर हुनुपर्दछ';
      },
      autofocus: false,
      maxLines: 10,
      decoration: InputDecoration(
          hintText: 'प्रतिक्रिया / सुझाव',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );

    final submitButton = Padding(
      
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(32.0),
        elevation: 5.0,
        shadowColor: Colors.lightBlueAccent.shade100,
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width * 3 / 4,
          height: 48.0,
          onPressed: () async {
            String errText='';
            _formKey.currentState.save();
            if (_formKey.currentState.validate()) {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("डेटा प्रशोधन हुँदैछ.."),
                      content: LinearProgressIndicator(),
                    );
                  });
              dio.options.connectTimeout = 15000; //15s
              dio.options.receiveTimeout = 15000;

              FormData formData = new FormData.from(_formdata);

              try {
                response = await dio.post('http://103.69.125.116/api/feedback',
                    data: formData);
              } on DioError catch (e) {
               print(e.message);
               errText=e.message.substring(0,68)+')';

              }
              if (response.statusCode.toString() == '200') {
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("सफलतापूर्वक पेश गरियो"),
                        content: Text('तपाईंको प्रतिक्रियाको लागि धन्यवाद'),
                      );
                    });
                Timer(Duration(milliseconds: 2000), () {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                });
              } else {
                Navigator.pop(context);
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("ERROR !!"),
                        content: Text(errText),
                        actions: <Widget>[
                          OutlineButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text('पुन: प्रयास गर्नुहोस्'),
                          )
                        ],
                      );
                    });
              }
            }
          },
          child: Text(
            "पेश गर्नुहोस्",
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.green[800],
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("प्रतिक्रिया / सुझाव "),
      ),
      backgroundColor: Colors.white,
      body: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 18.0, right: 18.0),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(12.0),
            ),
            logo,
            SizedBox(
              height: 28.0,
            ),
            Text(
              'यदि तपाइँसँग कुनै सुझाव वा प्रतिक्रिया छ भने कृपया तालको फारम भर्नुहोस् र पेश गर्नुहोस्मा ट्याप गर्नुहोस्\n\nतपाईंको प्रतिक्रिया हाम्रो लागि महत्त्वपूर्ण छ र हामी सकेसम्म चाँडै समाधान गर्न प्रयास गर्नेछौं',
              style: TextStyle(fontSize: 17.0),
            ),
            SizedBox(
              height: 28.0,
            ),
            nameField,
            SizedBox(
              height: 18.0,
            ),
            messageField,
            SizedBox(
              height: 8.0,
            ),
            submitButton,
            SizedBox(
              height: 64.0,
            ),
          ],
        ),
      ),
    );
  }
}
