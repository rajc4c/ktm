// import 'package:flutter/material.dart';
// import 'package:flutter_sms/flutter_sms.dart';

// class SendSMS extends StatefulWidget {
//   @override
//   _SendSMSState createState() => _SendSMSState();
// }

// class _SendSMSState extends State<SendSMS> {
//   String address = "9863333191";
//   final _formKey = GlobalKey<FormState>();
//   String _formdata = '';
//   bool _switchValue = true;

//   void _sendSMS() async {
//     String _result =
//         await FlutterSms.sendSMS(message: _formdata, recipients: [address])
//             .catchError((onError) {
//       print(onError);
//       showDialog(
//           context: context,
//           barrierDismissible: true,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Text("SMS Error"),
//               content: Text("Error Sending SMS"),
//             );
//           });
//     });
//     print(_result);
//     Navigator.pop(context);
//     showDialog(
//         context: context,
//         barrierDismissible: true,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             title: Text("SMS पठायो..."),
//             content: ListTile(
//               leading: Icon(Icons.done),
//               title: Text('SMS सफलतापूर्वक पठाइयो'),
//             ),
//           );
//         });
//   }

//   // _sendSMS() {
//   //   SmsMessage message = new SmsMessage(address, _formdata);
//   //   sender.sendSms(message);
//   //   message.onStateChanged.listen((state) {
//   //     if (state == SmsMessageState.Sent) {
//   //       Navigator.pop(context);
//   //       showDialog(
//   //           context: context,
//   //           barrierDismissible: true,
//   //           builder: (BuildContext context) {
//   //             return AlertDialog(
//   //               title: Text("SMS पठायो..."),
//   //               content: ListTile(
//   //                 leading: Icon(Icons.done),
//   //                 title: Text('SMS सफलतापूर्वक पठाइयो'),
//   //               ),
//   //             );
//   //           });
//   //     } else {
//   //       showDialog(
//   //           context: context,
//   //           barrierDismissible: true,
//   //           builder: (BuildContext context) {
//   //             return AlertDialog(
//   //               title: Text("SMS पठाउँदै..."),
//   //               content: LinearProgressIndicator(),
//   //             );
//   //           });
//   //     }
//   //   });
//   // }

//   @override
//   Widget build(BuildContext context) {
//     final registerbutton = Padding(
//       padding: EdgeInsets.symmetric(vertical: 16.0),
//       child: Material(
//         borderRadius: BorderRadius.circular(32.0),
//         elevation: 5.0,
//         shadowColor: Colors.lightBlueAccent.shade100,
//         child: MaterialButton(
//           minWidth: 200.0,
//           height: 48.0,
//           onPressed: () {
//             _formKey.currentState.save();
//             if (_formKey.currentState.validate() && _switchValue) {
//               _sendSMS();
//             } else if (_formKey.currentState.validate() && !_switchValue) {
//               showDialog(
//                   context: context,
//                   barrierDismissible: true,
//                   builder: (BuildContext context) {
//                     return AlertDialog(
//                       title: Text("तपाईंले नियम र सर्तहरू स्वीकार गर्नुपर्छ"),
//                       content: Text(
//                           "पेश गर्नु अघि हाम्रा सर्तहरू र सेवाहरू पढ्नुहोस् र स्वीकार गर्नुहोस्"),
//                       actions: <Widget>[
//                         OutlineButton(
//                           onPressed: () {
//                             setState(() {
//                               _switchValue = true;
//                             });

//                             Navigator.pop(context);
//                           },
//                           child: Text("स्वीकार्नुहोस्"),
//                         ),
//                         OutlineButton(
//                           onPressed: () {
//                             //_switchValue=true;
//                             Navigator.pop(context);
//                           },
//                           child: Text("रद्द गर्नुहोस्"),
//                         ),
//                       ],
//                     );
//                   });
//             }
//           },
//           child: Text(
//             "एसएमएस पठाउनुहोस्",
//             style: TextStyle(color: Colors.white),
//           ),
//           color: Colors.lightBlueAccent,
//         ),
//       ),
//     );

//     final name = TextFormField(
//       onSaved: (val) {
//         _formdata += 'name: ' + val;
//       },
//       validator: (value) {
//         if (value.isEmpty) {
//           return 'कृपया आफ्नो नाम लेख्नुहोस्';
//         }
//       },
//       autofocus: false,
//       decoration: InputDecoration(
//           hintText: 'पुरा नाम',
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border:
//               OutlineInputBorder(borderRadius: BorderRadius.circular((10.0)))),
//     );
//     final title = TextFormField(
//       onSaved: (val) {
//         _formdata += '\ntitle: ' + val;
//       },
//       validator: (value) {
//         if (value.isEmpty) {
//           return 'कृपया मुद्दाको लागि शीर्षक प्रविष्ट गर्नुहोस्';
//         }
//       },
//       autofocus: false,
//       decoration: InputDecoration(
//           hintText: 'मुद्दाको शीर्षक',
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border:
//               OutlineInputBorder(borderRadius: BorderRadius.circular((10.0)))),
//     );
//     final description = TextFormField(
//       maxLines: 5,
//       onSaved: (val) {
//         _formdata += '\ndescription: ' + val;
//       },
//       validator: (value) {
//         if (value.isEmpty) {
//           return 'कृपया मुद्दा विवरणमा कम्तिमा 30 अक्षरहरू विस्तार गर्नुहोस्';
//         }
//       },
//       autofocus: false,
//       decoration: InputDecoration(
//           hintText: 'कृपया मुद्दा विवरणमा विस्तार गर्नुहोस्',
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border:
//               OutlineInputBorder(borderRadius: BorderRadius.circular(14.0))),
//     );

//     return Scaffold(
//       appBar: AppBar(
//         title: Text("एसएमएस मार्फत दर्ता गर्नुहोस्"),
//       ),
//       body: Form(
//         key: _formKey,
//         child: ListView(
//           padding: EdgeInsets.all(9.0),
//           shrinkWrap: true,
//           children: <Widget>[
//             SizedBox(
//               height: 18.0,
//             ),
//             Text(
//               "कृपया CIAA मा मुद्दा पेश गर्न फारम तल भर्नुहोला। निश्चित गर्नुहोस् कि विवरणहरू सटीक र विस्तृत छन्",
//               softWrap: true,
//             ),
//             SizedBox(
//               height: 18.0,
//             ),
//             name,
//             SizedBox(
//               height: 18.0,
//             ),
//             title,
//             SizedBox(
//               height: 18.0,
//             ),
//             description,
//             SizedBox(
//               height: 18.0,
//             ),
//             registerbutton
//           ],
//         ),
//       ),
//     );
//   }
// }
