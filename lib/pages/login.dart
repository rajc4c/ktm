import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'forgotpassword.dart';
import 'register.dart';
import '../helper/pref.dart';
import 'package:dio/dio.dart';

Dio dio = new Dio();
Response response = Response();
Map<String, dynamic> _formdata = {};

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool sucess = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final logo = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Hero(
          tag: 'hero',
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            //backgroundImage: AssetImage('static/logo/logo.png'),
            radius: 50.0,
            child: Image.asset('static/logo/logo.png'),
          ),
        ),
        Hero(
          tag: 'hero1',
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            //backgroundImage: AssetImage('static/logo/logo.png'),
            radius: 50.0,
            child: Image.asset('static/logo/ciaa.png'),
          ),
        ),
      ],
    );
    final email = TextFormField(
      onSaved: (val) {
        _formdata['email'] = val;
      },
      validator: (value) {
        Pattern pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regex = new RegExp(pattern);
        if (!regex.hasMatch(value) && value.length != 10) {
          return 'ई-मेल वा मोबाइल नम्बर मान्य छैन';
        }
      },
      // keyboardType: TextInputType.emailAddress,
      autofocus: false,
      // initialValue: 'myemailid@gmail.com',
      decoration: InputDecoration(
          hintText: 'इमेल / मोबाइल नम्बर',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );
    final password = TextFormField(
      onSaved: (val) {
        _formdata['password'] = val;
      },
      validator: (value) {
        if (value.length < 6) return 'पासवर्ड कम्तिमा 6 अक्षर हुनुपर्दछ';
      },
      obscureText: true,
      autofocus: false,
      // initialValue: 'my password',
      decoration: InputDecoration(
          hintText: 'पासवर्ड',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(34.0))),
    );

    final loginbutton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(32.0),
        elevation: 5.0,
        shadowColor: Colors.lightBlueAccent.shade100,
        child: MaterialButton(

          minWidth: MediaQuery.of(context).size.width*3/4,
          height: 48.0,
          onPressed: () async {
            _formKey.currentState.save();
            if (_formKey.currentState.validate()) {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("डेटा प्रशोधन हुँदैछ.."),
                      content: LinearProgressIndicator(),
                      
                    );
                  });
              dio.options.connectTimeout = 15000; //15s
              dio.options.receiveTimeout = 15000;

              FormData formData = new FormData.from(_formdata);
              var d;
              var name = '';
              response = await dio.post(
                  'http://103.69.125.116/api/login_api',
                  data: formData);
              if (response.data.toString() !=
                  'Login Failed...Please Try Again!!') {
                d = json.decode(response.data);
                for (Map dat in d) {
                  String id = dat['id'].toString();
                  name = dat['name'];
                            String phone = dat['phone'].toString();
                  setsharedInstance(uid: id);
                  setsharedemail(email: dat['email']);
                  setsharedname(name: dat['name']);
                  setsharedaddress(add: dat['address']);
                  setsharedphone(phone: phone);
                  
                }
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("सफलत"),
                        content: Text('$name स्वागत छ'),
                      );
                    });
                Timer(Duration(milliseconds: 2000), () {
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                });
              }
              else {
                Navigator.pop(context);
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("त्रुटि !!"),
                        content: Text('प्रयोगकर्तानाम पासवर्ड मेल खाँदैन'),
                        actions: <Widget>[
                        OutlineButton(onPressed: ()=>Navigator.pop(context),child: Text('पुन: प्रयास गर्नुहोस्'),)
                      ],
                      );
                    });
              }
            }
          },
          child: Text(
            "लग इन",
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.green[800],
        ),
      ),
    );

    final forgot = FlatButton(
      
      child: Text(
        "पासवर्ड बिर्सनुभयो ?",
        style: TextStyle(color: Colors.black),
      ),
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ForgotPassword()));
      },
    );
    final newaccount = OutlineButton(
      child: Text(
        "नयाँ खाता खोल्नुहोस् ",
        style: TextStyle(color: Colors.black),
      ),
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => RegisterPage()));
      },
    );
    final guest = OutlineButton(
      child: Text(
        "लगइन बिना प्रयोग गर्नुहोस्",
        style: TextStyle(color: Colors.black),
      ),
      onPressed: () {
        Navigator.popUntil(context, ModalRoute.withName('/'));
        // Navigator.push(
        //     context, MaterialPageRoute(builder: (context) => CiaaApp()));
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("लगइन / नयाँ खाता खोल्नुहोस् "),
      ),
      backgroundColor: Colors.white,
      body: Form(
          key: _formKey,
          child: ListView(
          
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              Padding(padding: EdgeInsets.all(50.0),),
      logo,
      SizedBox(
        height: 38.0,
      ),
      email,
      SizedBox(
        height: 18.0,
      ),
      password,
      SizedBox(
        height: 8.0,
      ),
      loginbutton,
      SizedBox(
        height: 24.0,
      ),
      forgot,
             SizedBox(
        height: 64.0,
      ),
       Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[newaccount, guest],
         )
            ],
          ),
        ),
    );
  }
}
