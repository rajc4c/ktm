import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:documents_picker/documents_picker.dart';
// import 'package:location/location.dart';

Map<String, dynamic> _formdata = {};
String district = 'जिल्ला चयन गर्नुहोस्';
String office = 'कार्यालय चयन गर्नुहोस्', fileName='फाइल अपलोड गर्न यहाँ थिच्नुहोस्';
  final _formKey1 = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  bool _switchValue = true;
  String department = '';
  File _image;
  File _video;
  
TextEditingController controller = new TextEditingController();
TextEditingController controller1 = new TextEditingController();

class PostIssuePage extends StatefulWidget {
  @override
  _PostIssuePageState createState() => _PostIssuePageState();
}

class _PostIssuePageState extends State<PostIssuePage> {

TextEditingController emailcontroller = TextEditingController();
TextEditingController namectl = TextEditingController();
TextEditingController phonectl = TextEditingController();

  // districtpop(BuildContext context) async {
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: true,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           content: ShowDist(),
  //         );
  //       });
  // }

  // officepop(BuildContext context) async {
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: true,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           content: ShowOffice(),
  //         );
  //       });
  // }

 Future getImageCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
      int len = image.toString().length;
      var name = image.toString().substring(len - 20, len - 1);
    
      _formdata['image'] = UploadFileInfo(_image, name);
      //imagecount++;
      Navigator.pop(context);
    });
  }
   _openPop(){
     return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(10.0),
            content: Text('कृपया तस्विर छान्नुहोस्'),
             actions: <Widget>[

               OutlineButton(
                 onPressed: getImageGallery,
                 child: Text('ग्यालरी',style: TextStyle(fontSize: 24.0),),
               ),
               OutlineButton(
                  onPressed: getImageCamera,
                 child: Text('क्यामेरा',style: TextStyle(fontSize: 24.0),),
               )
             ],
          );
        });

  }
    _openVideoPop(){
     return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(10.0),
            content: Text('कृपया भिडियो छान्नुहोस्'),
             actions: <Widget>[

               OutlineButton(
                 onPressed: getVideoGallery,
                 child: Text('ग्यालरी',style: TextStyle(fontSize: 24.0),),
               ),
               OutlineButton(
                  onPressed: getVideoCamera,
                 child: Text('क्यामेरा',style: TextStyle(fontSize: 24.0),),
               )
             ],
          );
        });

  }
  Future getImageGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      int len = image.toString().length;
      var name = image.toString().substring(len - 20, len - 1);
    
      _formdata['image'] = UploadFileInfo(_image, name);
      //imagecount++;
      Navigator.pop(context);
    });
  }
  Future getVideoGallery() async {
    var video = await ImagePicker.pickVideo(source: ImageSource.gallery);

    setState(() {
      _video = video;
      int len = video.toString().length;
      var name = video.toString().substring(len - 20, len - 1);
    
      _formdata['video'] = UploadFileInfo(_video, name);
      //imagecount++;
      Navigator.pop(context);
    });
  }

 Future getVideoCamera() async {
    var video = await ImagePicker.pickVideo(source: ImageSource.camera);

    setState(() {
      _video = video;
      int len = video.toString().length;
      var name = video.toString().substring(len - 20, len - 1);
    
      _formdata['video'] = UploadFileInfo(_video, name);
      //imagecount++;
      Navigator.pop(context);
    });
  }
   Future getFile() async {
    // List<dynamic> docPaths = await DocumentsPicker.pickDocuments;

    setState(() {
     // _image = image;
      // int len = docPaths[0].toString().length;
      //  fileName = docPaths[0].toString().substring(len - 20, len);
    
      // _formdata['otherfile'] = UploadFileInfo(File(docPaths[0]), fileName);
      //imagecount++;
    });
  }

  // Future<Null> getOfficeDetails() async {
  //   final response = await http.get(url1);
  //   final responseJson = json.decode(response.body);

  //   setState(() {
  //     for (Map user in responseJson) {
  //       _officeNames.add(OfficeDetails.fromJson(user));
       
  //     }
  //   });
  // }

  // Future<Null> getministry(id) async {
  //   final response =
  //       await http.get('http://103.69.125.116/api/send_ministry?id=$id');
  //   final responseJson = json.decode(response.body);

  //   setState(() {
  //     for (Map user in responseJson) {
  //       department = user['name'];
  //       _formdata['ministry_id'] = user['id'];
  //     }
  //   });
  //   setState(() {});
  // }

  // Future<Null> getdistrictDetailss() async {
  //   final response = await http.get(url);
  //   final responseJson = json.decode(response.body);

  //   setState(() {
  //     for (Map user in responseJson) {
  //       //print(user);
  //       _districtName.add(districtDetailss.fromJson(user));
        
  //     }
  //   });
  // }

  Future<Null> getGeoLocation() async {
    // Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    var currentLocation ;

// var location = new Location();
// currentLocation = await location.getLocation();

    print(currentLocation);
  
    _formdata['latitude'] = currentLocation['latitude'];
    _formdata['longitude'] = currentLocation['longitude'];
    setState(() {});
  }

  @override
  void initState() {
    getGeoLocation();
    // getdistrictDetailss();
    // getOfficeDetails();
    super.initState();
    _formdata['name']='Anonymous';
    _formdata['email']='Anonymous@user.com';
    _formdata['number']='0000000000';
  }
  @override
    void dispose() {
      fileName='फाइल अपलोड गर्न यहाँ थिच्नुहोस्';
      super.dispose();
    }

  @override
  Widget build(BuildContext context) {


 //file selector

    final fileselect = Container(
        decoration: BoxDecoration(
            border: Border.all(
                color: Colors.grey, style: BorderStyle.solid, width: 1.0),
            borderRadius: BorderRadius.circular(12.0)),
        width: double.maxFinite,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: MaterialButton(
            onPressed: getFile,
            child: Text(fileName),
          ),
        )
        );
    //image selector
    final selectimage = Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        border: Border.all(
            color: Colors.grey, style: BorderStyle.solid, width: 1.0),
      ),
      child: ListTile(
        contentPadding: EdgeInsets.all(8.0),
        trailing: Container(
          child: _image != null
              ? Column(
                  children: <Widget>[
                    IconButton(
                      onPressed: _openPop,
                      icon: Icon(Icons.image),
                    ),
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        setState(() {
                          _image = null;
                        });
                      },
                    ),
                  ],
                )
              : IconButton(
                onPressed: _openPop,
                icon: Icon(Icons.image),
              ),
        ),
        // // onPressed:getImage,
        //   leading: _image != null
        //     ? IconButton(
        //       icon: Icon(Icons.camera),
        //       onPressed: getImageCamera,
        //     )
        //     : IconButton(
        //       icon: Icon(Icons.camera),
        //       onPressed: getImageCamera,),
        title: _image != null
            ? Image.file(
                _image,
                width: double.maxFinite,
              )
            : GestureDetector(
              onTap: _openPop,
                          child: Container(
                  child: Text("फोटो अपलोड गर्न यहाँ क्लिक गर्नुहोस्",textAlign: TextAlign.center,),
                ),
            ),
        //onTap: getImage,
      ),
    );


final videoSelect = Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        border: Border.all(
            color: Colors.grey, style: BorderStyle.solid, width: 1.0),
      ),
      child: ListTile(
        contentPadding: EdgeInsets.all(8.0),
        trailing: Container(
          child: _video != null
              ? Column(
                  children: <Widget>[
                    IconButton(
                      onPressed: _openVideoPop,
                      icon: Icon(Icons.videocam),
                    ),
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        setState(() {
                          _video = null;
                        });
                      },
                    ),
                  ],
                )
              : IconButton(
                onPressed: _openVideoPop,
                icon: Icon(Icons.videocam),
              ),
        ),
        title: _video != null
            ? Text('Video Selected')
            : GestureDetector(
              onTap: _openVideoPop,
                          child: Container(
                  child: Text("भिडियो अपलोड गर्न यहाँ क्लिक गर्नुहोस्",textAlign: TextAlign.center,),
                ),
            ),
        //onTap: getImage,
      ),
    );

//department auto populate
    final concerneddepartment = department == ''
        ? Container()
        : Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                borderRadius: BorderRadius.circular(12.0)),
            width: double.maxFinite,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                department,
                softWrap: true,
                style: TextStyle(fontSize: 18.0),
              ),
            ),
          );

//office selector pop
    // final selectoffice = Container(
    //     decoration: BoxDecoration(
    //         border: Border.all(
    //             color: Colors.grey, style: BorderStyle.solid, width: 1.0),
    //         borderRadius: BorderRadius.circular(12.0)),
    //     width: double.maxFinite,
    //     child: Padding(
    //       padding: const EdgeInsets.symmetric(horizontal: 16.0),
    //       child: MaterialButton(
    //         onPressed: () async {
    //           await officepop(context);
    //           setState(() {
    //             getministry(_formdata['bivag_id']);
    //           });
    //         },
    //         child: Text(office),
    //       ),
    //     ));

//name input field
    final name = TextField(
      controller: namectl,
      onChanged: (val){
        _formdata['name']=val;
      },
      
      // onSaved: (val) {
      //   _formdata['name'] = val;
      // },
      // // initialValue: 'Anonymous',
      // validator: (value) {
      //   if (value.isEmpty) {
      //     return 'कृपया आफ्नो नाम उल्लेख गर्नुहोस्';
      //   }
      // },
      autofocus: false,
      
      decoration: InputDecoration(
          hintText: 'पुरा नाम',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular((10.0)))),
    );

//issue title input
    final heading = TextFormField(
      //autovalidate: true,
      autocorrect: false,
      //onFieldSubmitted: (val)=>_formKey1.currentState.save(),
      onSaved: (val) {
       
        _formdata['title'] = val;
      },
      validator: (value) {
        if (value.isEmpty) {
          return 'गुनासोको शिर्षक लेख्नुहोस्';
        }
      },
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'गुनासोको शीर्षक',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular((10.0)))
              ),
    );

//issue message detail description
    final description = TextFormField(  
      maxLines: 5,
      onSaved: (val) {
        _formdata['message'] = val;
      },
      validator: (value) {
        if (value.isEmpty) {
          return 'आफ्नो गुनासो बिस्तृतमा उल्लेख गर्नुहोस् (न्यूनतम ३० शब्दमा )';
        }
      },
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'कृपया आफ्नो गुनासो बिस्तृतमा लेख्नुहोस्',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(14.0))),
    );

//mobile number input field
    final mobile = TextField(
      controller: phonectl,
      onChanged: (val){
        _formdata['number'] = val;

      },
      // onSaved: (val) {
      //   _formdata['number'] = val;
      // },
      // // initialValue: '0000000000',
      // validator: (value) {
      //   if (value.length != 10) {
      //     return 'कृपया वैधानिक मोबाइल नम्बर राख्नुहोस्';
      //   }
      // },
      keyboardType: TextInputType.phone,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'मोबाइल नम्बर',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular((10.0)))),
    );
    final email = TextField(
      // onSaved: (val) {
      //   _formdata['email'] = val;
      // },
      // // initialValue: 'user@user.com',
      // validator: (value) {
      //   if (value.isNotEmpty) {
      //     Pattern pattern =
      //         r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      //     RegExp regex = new RegExp(pattern);
      //     if (!regex.hasMatch(value)) {
      //       return 'ई-मेल वा मोबाइल नम्बर मान्य छैन';
      //     }
      //   }
      // },
      controller: emailcontroller,
      autocorrect: false,
      onChanged: (val){
        _formdata['email']=val;

      },
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      // initialValue: 'myemailid@gmail.com',
      decoration: InputDecoration(
          hintText: 'इमेल ठेगाना (वैकल्पिक)',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular((10.0)))),
    );

    //district selection field 
    // final selectdistrict = Container(
    //     decoration: BoxDecoration(
    //         border: Border.all(
    //             color: Colors.grey, style: BorderStyle.solid, width: 1.0),
    //         borderRadius: BorderRadius.circular(12.0)),
    //     width: double.maxFinite,
    //     child: Padding(
    //       padding: const EdgeInsets.symmetric(horizontal: 16.0),
    //       child: MaterialButton(
    //         onPressed: () async {
    //           await districtpop(context);
    //           setState(() {});
    //         },
    //         child: Text(district),
    //       ),
    //     ));


        //status indicator after submitting issue
    showprogress(String txt, String msg) {
      if(txt.isNotEmpty){
       // Navigator.pop(context);
          showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(msg),
              content: txt.isNotEmpty ? Text(txt) : LinearProgressIndicator(),
              actions: <Widget>[
                OutlineButton(
                  onPressed: (){
                    //Navigator.pop(context);
                   // Navigator.pop(context);
                    Navigator.popUntil(context, ModalRoute.withName('/'));
                  },
                  child: Text('बन्द गर्नुहोस्'),
                )
              ],
            );
          });
      }
      else{
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(msg),
              content: txt.isNotEmpty ? Text(txt) : LinearProgressIndicator(),
            );
          });
      }
      
    }

//resgister button
    final registerbutton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(32.0),
        elevation: 5.0,
        shadowColor: Colors.lightBlueAccent.shade100,
        child: MaterialButton(
          minWidth: 200.0,
          height: 48.0,
          onPressed: () async {
            
            var dio = new Dio();
            //dio.options.baseUrl = "http://103.69.125.116/api";
            dio.options.connectTimeout = 15000; //15s
            dio.options.receiveTimeout = 15000;
            _formKey1.currentState.save();
           // _formKey2.currentState.save();
           //print(_formdata);
            
            if (_formKey1.currentState.validate()&& _switchValue) {
              Response response = Response();
              String sucess = '';
              var msg = "डेटा प्रशोधन हुँदैछ...";
              showprogress(sucess, msg);
              FormData formData = new FormData.from(_formdata);
                print(formData);
              response = await dio.post(
                  "https://c4cprojects.com/ktm/public/api/issuestore",
                  data: formData);
              setState(() {
                print(response);
                sucess=response.data;
                msg='सफलता !!';
               showprogress(sucess, msg);
              });
            } else if (_formKey1.currentState.validate() && !_switchValue) {
               showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("तपाईंले नियम र सर्तहरू स्वीकार गर्नुपर्छ"),
                      content: Text(
                          "पेश गर्नु अघि हाम्रा सर्तहरू र सेवाहरू पढ्नुहोस् र स्वीकार गर्नुहोस्"),
                      actions: <Widget>[
                        OutlineButton(
                          onPressed: () {
                            setState(() {
                              _switchValue = true;
                            });

                            Navigator.pop(context);
                          },
                          child: Text("स्वीकार्नुहोस्"),
                        ),
                        OutlineButton(
                          onPressed: () {
                            //_switchValue=true;
                            Navigator.pop(context);
                          },
                          child: Text("रद्द गर्नुहोस्"),
                        ),
                      ],
                    );
                  });
            }
          },
          child: Text(
            "पेश गर्नुहोस्",
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.lightBlueAccent,
        ),
      ),
    );

//building layout here..
    return Scaffold(
      appBar: AppBar(
        title: Text("काठमाडौं महानगरपालिका"),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
        children: <Widget>[
          
          infotext(),
          SizedBox(
            height: 18.0,
          ),
          Form(
            key: _formKey2,
                      child: Column(
              children: <Widget>[
                name,
                SizedBox(
            height: 18.0,
          ),
                mobile,
          SizedBox(
            height: 18.0,
          ),
          email,
          SizedBox(
            height: 18.0,
          ),
              ],
            ),
          ),
          
          Text(
            "मुद्दाको विवरण",
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          Divider(),
          Text(
            "काठमाडौं महानगरपालिकामा आफ्नो गुनासो पेश गर्न यो फारम भरेर बुझाउनुहोस्। कृपया आफूले पेश गरेको विवरण दुरुस्त रहेको यकिन गर्नुहोस्।",
            softWrap: true,
          ),
          SizedBox(
            height: 18.0,
          ),
          Form(
            key: _formKey1,

            child: Column(
              children: <Widget>[
                heading,
                SizedBox(
            height: 18.0,
          ),
          description,
              ],
            )
            ),
          
          SizedBox(
            height: 18.0,
          ),
          selectimage,
          SizedBox(
            height: 18.0,
          ),
           videoSelect,
          SizedBox(
            height: 18.0,
          ),
          fileselect,
          SizedBox(
            height: 18.0,
          ),
          // selectdistrict,
          // SizedBox(
          //   height: 18.0,
          // ),
          // selectoffice,
          // SizedBox(
          //   height: 18.0,
          // ),
          concerneddepartment,
          SizedBox(
            height: 18.0,
          ),
          
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Switch(
                value: _switchValue,
                onChanged: (bool value) {
                  setState(() {
                    _switchValue = value;
                  });
                },
              ),
              Expanded(
                  child: Text(
                "तलको 'पेश गर्नुहोस्' बटन क्लिक गर्नुभएको अवस्थामा तपाईले प्रयोगका नियम र सर्तहरू स्वीकार गर्नुभएको छ।",
                softWrap: true,
              )),
              GestureDetector(
                onTap: () {
                   showDialog(
                      context: context,
                      barrierDismissible: true,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text("प्रयोगको नियम तथा सर्तहरू"),
                          content: SingleChildScrollView(
                            child: Text(
                              "तपाईको व्यक्तिगत विवरण गोप्य राखिनेछ। पेश गरिएका विवरणको प्रमाणीकरण र बैधानिकता बुझ्न अख्तियार दुरूपयोग अनुसन्धान आयोगले यी सूचना प्रयोग गर्न सक्नेछ। तपाईले बुझाएका विवरणको सत्यता बुझ्न र गुनासोऊपर उचित कारवाहीको प्रक्रिया अघि बढाउन उल्लेख भएका सम्पर्क माध्यमबाट थप सूचना आदान प्रदानको लागि गर्न का.म.न.पा.ले तपाईलाई सम्पर्क गर्न सक्नेछ ।",
                              softWrap: true,
                            ),
                          ),
                          actions: <Widget>[
                            MaterialButton(
                              onPressed: () => Navigator.pop(context),
                              child: Text("बन्द गर्नुहोस्"),
                            )
                          ],
                        );
                      });
                },
                child: Text(
                  "नियम र सर्तहरू",
                  style: TextStyle(color: Colors.blue),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 18.0,
          ),
          registerbutton,
          SizedBox(
            height: 24.0,
          ),
        ],
      ),
    );
  }
}

// List<districtDetailss> _districtSearchResult = [];
// List<districtDetailss> _districtName = [];

// final String url = 'http://103.69.125.116/api/send_district';

// class districtDetailss {
//   final int id;
//   final String name, nepaliname;

//   districtDetailss({
//     this.id,
//     this.name,
//     this.nepaliname,
//   });

//   factory districtDetailss.fromJson(Map<String, dynamic> json) {
//     return new districtDetailss(
//       id: json['id'],
//       name: json['name'],
//       nepaliname: json['nepaliname'],
//     );
//   }
// }

// class ShowDist extends StatefulWidget {
//   @override
//   _ShowDistState createState() => _ShowDistState();
// }

// class _ShowDistState extends State<ShowDist> {
//   onSearchTextChanged(String text) async {

//     _districtSearchResult.clear();
//     if (text.isEmpty) {
//       setState(() {});
//       return;
//     }

//     _districtName.forEach((districtDetails) {
//       if (districtDetails.name.isNotEmpty) {
//         if (districtDetails.name.toLowerCase().contains(text.toLowerCase()))
//           setState(() {
//             _districtSearchResult.add(districtDetails);
//           });
//       }
//     });

//     setState(() {});
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: double.maxFinite,
//       child: Column(children: <Widget>[
//         Container(
//           child: new Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: new Card(
//               child: new ListTile(
//                 leading: new Icon(Icons.search),
//                 title: new TextField(
//                   autocorrect: false,
//                   controller: controller,
//                   decoration: new InputDecoration(
//                       hintText: 'खोज', border: InputBorder.none),
//                   onChanged: (txt) {
//                     onSearchTextChanged(txt);
//                     setState(() {});
//                   },
//                 ),
//                 trailing: new IconButton(
//                   icon: new Icon(Icons.cancel),
//                   onPressed: () {
//                     controller.clear();
//                     onSearchTextChanged('');
//                   },
//                 ),
//               ),
//             ),
//           ),
//         ),
//         Expanded(
//           child: _districtSearchResult.length != 0 || controller.text.isNotEmpty
//               ? ListView.builder(
//                   itemCount: _districtSearchResult.length,
//                   itemBuilder: (context, i) {
//                     return ListTile(
//                         onTap: () {
//                           _formdata['district_id'] =
//                               _districtSearchResult[i].id;
//                           Navigator.pop(context);
//                           district = _districtSearchResult[i].name;
//                         },
//                         title: Text(_districtSearchResult[i].name));
//                   })
//               : ListView.builder(
//                   itemCount: _districtName.length,
//                   itemBuilder: (context, i) {
//                     return ListTile(
//                         onTap: () {
//                           _formdata['district_id'] = _districtName[i].id;
//                           district = _districtName[i].name;
//                           Navigator.pop(context);
//                         },
//                         title: Text(_districtName[i].name));
//                   }),
//         ),
//       ]),
//     );
//   }
// }

// class ShowOffice extends StatefulWidget {
//   @override
//   _ShowOfficeState createState() => _ShowOfficeState();
// }

// class _ShowOfficeState extends State<ShowOffice> {
//   onSearchTextChanged1(String text) async {
   
//     _officeSearchResult.clear();
//     if (text.isEmpty) {
//       setState(() {});
//       return;
//     }

//     _officeNames.forEach((officedetails) {
//       if (officedetails.name.isNotEmpty) {
//         if (officedetails.name.toLowerCase().contains(text.toLowerCase()))
//           setState(() {
//             _officeSearchResult.add(officedetails);
//           });
//       }
//     });

//     setState(() {});
//   }
// @override
//   void dispose() {
//     _districtSearchResult.clear();
//     _officeSearchResult.clear();
//     super.dispose();
//   }
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: double.maxFinite,
//       child: Column(children: <Widget>[
//         Container(
//           child: new Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: new Card(
//               child: new ListTile(
//                 leading: new Icon(Icons.search),
//                 title: new TextField(
//                   autocorrect: false,
//                   controller: controller1,
//                   decoration: new InputDecoration(
//                       hintText: 'खोज', border: InputBorder.none),
//                   onChanged: (txt) {
//                     onSearchTextChanged1(txt);
//                     setState(() {});
//                   },
//                 ),
//                 trailing: new IconButton(
//                   icon: new Icon(Icons.cancel),
//                   onPressed: () {
//                     controller1.clear();
//                     onSearchTextChanged1('');
//                   },
//                 ),
//               ),
//             ),
//           ),
//         ),
//         Expanded(
//           child: _officeSearchResult.length != 0 || controller1.text.isNotEmpty
//               ? ListView.builder(
//                   itemCount: _officeSearchResult.length,
//                   itemBuilder: (context, i) {
//                     return ListTile(
//                         onTap: () {
//                           _formdata['bivag_id'] = _officeSearchResult[i].id;
                          
//                           office = _officeSearchResult[i].name;
//                           Navigator.pop(context);
//                         },
//                         title: Text(_officeSearchResult[i].name));
//                   })
//               : ListView.builder(
//                   itemCount: _officeNames.length,
//                   itemBuilder: (context, i) {
//                     return ListTile(
//                         onTap: () {
//                           _formdata['bivag_id'] = _officeNames[i].id;
//                           office = _officeNames[i].name;
//                           Navigator.pop(context);
//                         },
//                         title: Text(_officeNames[i].name));
//                   }),
//         ),
//       ]),
//     );
//   }
// }

// List<OfficeDetails> _officeSearchResult = [];
// List<OfficeDetails> _officeNames = [];

// final String url1 = 'http://103.69.125.116/api/send_bivag';

// class OfficeDetails {
//   final int id;
//   final String name;

//   OfficeDetails({
//     this.id,
//     this.name,
//   });

//   factory OfficeDetails.fromJson(Map<String, dynamic> json) {
//     return new OfficeDetails(
//       id: json['id'],
//       name: json['name'],
//     );
//   }
// }



Widget infotext(){
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
              "व्यक्तिगत विवरण (वैकल्पिक)",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            Divider(),
            Text(
              "कृपया आफ्नो व्यक्तिगत विवरण उल्लेख गर्नुहोस्",
              softWrap: true,
              style: TextStyle(fontSize: 16.0),
            ),
            Text(
              "नोट: तपाईको व्यक्तिगत विवरण गोप्य राखिनेछ। पेश गरिएका विवरणको प्रमाणीकरण र बैधानिकता बुझ्न का.म.न.पा.ले यी सूचना प्रयोग गर्न सक्नेछ।",
              softWrap: true,
              style: TextStyle(fontStyle: FontStyle.italic, color: Colors.blue),
            ),
    ],
  );
}