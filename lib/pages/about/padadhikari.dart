import 'package:flutter/material.dart';

class PadAdhikari extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('पदाधिकारीहरु'),
      ),
      body: ListView(
        
        padding: EdgeInsets.all(10.0),
        shrinkWrap: true,
        children: <Widget>[
        buildBox('p1','माननीय कार्यवाहक प्रमुख आयुक्त नवीनकुमार घिमिरे'),
        buildBox('p2','माननीय आयुक्त डा. गणेशराज जोशी'),
        buildBox('p3','माननीय आयुक्त राजनारायण पाठक'),
        buildBox('p4','माननीय आयुक्त डा. सावित्री थापा गुरुङ'),
        buildBox('p5','सचिव महेश्वर न्यौपाने'),
        ],
      ),
    );
  }
}

Widget buildBox (img,name){
  return   Card(
    //shape: RoundedRectangleBorder(),
      child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset('static/img/$img.png'),
                  Divider(
                
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal:  8.0),
                    child: Text(name,style: TextStyle(fontSize: 20.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom:  8.0),
                    child: Text('काठमाडौं महानगरपालिका'),
                  )
                ],
              ),
  );
}