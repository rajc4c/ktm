import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

Future<String> getsharedInstance() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
   return  prefs.getString('id');
  
}
Future<String> getsharedemail() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return  prefs.getString('email');
  }
  
  Future<String> getsharedname() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return  prefs.getString('name');
  }
  Future<String> getshareadd() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return  prefs.getString('add');
  }
  Future<String> getsharedphone() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return  prefs.getString('phone');
  }

Future<bool> setsharedInstance({uid}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('id', uid).then((bool sucess){
    return sucess;
  });
  return false;
}
Future<bool> setsharedemail({email}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
await  prefs.setString('email', email).then((bool sucess){
    return sucess;
  });
  return false;
  
}

Future<bool> setsharedname({name}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
await  prefs.setString('name', name).then((bool sucess){
    return sucess;
  });
  return false;
  
}

Future<bool> setsharedaddress({add}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
await  prefs.setString('add', add).then((bool sucess){
    return sucess;
  });
  return false;
  
}

Future<bool> setsharedphone({phone}) async {
  // print(phone);
  SharedPreferences prefs = await SharedPreferences.getInstance();
await  prefs.setString('phone', phone).then((bool sucess){
    return sucess;
  });
  return false;
//  print('error');
  
}