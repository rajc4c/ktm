import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ImportantLinks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('महत्वपूर्ण लिंकहरू'),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          buildLink('ट्रान्सपरेन्सी इन्टरनेशनल','http://www.transparency.org/'),
          buildLink('एशियाली विकास बैंक','http://www.adb.org/'),
          buildLink('विश्व बैंक','http://www.worldbank.org/html/extdr/anticorruption'),
          buildLink('ट्रान्सपरेन्सी इन्टरनेशनल नेपाल','http://www.tinepal.org/'),
          buildLink('राष्ट्रिय योजना आयोग','http://www.npc.gov.np/'),
          buildLink('लोक सेवा आयोग','http://www.psc.gov.np/'),
          buildLink('प्रीतिवाट नेपाली यूनिकोडमा','https://www.nepali-unicode.com/preeti-to-nepali-unicode/'),
          buildLink('यूनिकोड बाट प्रीतिमा','http://unicode.shresthasushil.com.np/'),
          buildLink('SOCH Nepal','https://www.sochnepal.org/'),
          buildLink('App Developer','https://www.codeforcore.com/'),
        
        ],
      ),
      
    );
  }
}

Widget buildLink( String name, String link){
  return ListTile(
    leading: Icon(Icons.link),
    onTap: () async {
              //Navigator.pop(context);
              var url = link;

              if (await canLaunch(url)) {
                
                await launch(url,forceWebView: true);
              } else {
                throw 'Could not launch $url';
              }
            },
    title:Text('$name') ,
    subtitle: Text('$link',style: TextStyle(color: Colors.blue),softWrap: true,overflow: TextOverflow.ellipsis,),
  );
}


